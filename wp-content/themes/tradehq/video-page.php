<?php
//Template Name: Video page

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'wst_display_videos' );
function wst_display_videos() {
	$context   = Timber::get_context();
	$templates = array( 'videos.twig' );
	Timber::render( $templates, $context );
}


genesis();