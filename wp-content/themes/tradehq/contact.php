<?php
//Template Name: Contact page

remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

add_action( 'genesis_after_header', 'wst_display_gmaps' );
function wst_display_gmaps() { ?>
    <div class="gmaps">
	    <?php echo wp_kses_post(the_field('gmaps'));?>
    </div>
<?php }

add_action( 'genesis_after_content', 'wst_display_contact_info' );
function wst_display_contact_info() {
	$context   = Timber::get_context();
	$context['form'] = do_shortcode('	[ninja_form id=3]');
	$templates = array( 'contact.twig' );
	Timber::render( $templates, $context );


}

genesis();