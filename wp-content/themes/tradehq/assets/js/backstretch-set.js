jQuery(function( $ ){

    $(".header-wrap").backstretch([BackStretchImg.src],{
        duration:2000,
        fade:750,
        overlay: {
            init: false,
            background: "#01384D",
            opacity: 0.4
        }
    });

});