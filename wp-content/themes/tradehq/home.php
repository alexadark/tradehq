<?php

remove_action('genesis_entry_header','genesis_post_info', 12);
remove_action('genesis_entry_footer','genesis_post_meta');

add_filter( 'genesis_attr_content', 'wst_change_content_attr', 99 );
function wst_change_content_attr( $attr ) {
	$attr['class']   .= ' uk-child-width-1-3@m uk-grid';

	return $attr;
}


// Edit the read more link text
add_filter('get_the_content_more_link', 'custom_read_more_link');
add_filter('the_content_more_link', 'custom_read_more_link');
function custom_read_more_link() {
	return '...&nbsp;<a class=" uk-button uk-button-default more-link uk-margin-top uk-display-block" href="' . get_permalink() . '" rel="nofollow">Read more</a>';
}


genesis();