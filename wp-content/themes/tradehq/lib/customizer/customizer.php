<?php
/**
 * Webstantly theme customizer
 *
 * @package Webstantly Starter theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wst_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'wst_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'wst_customize_partial_blogdescription',
		) );
	}



	$wp_customize->add_section(
		'wst_additional_options',
		array(
			'title'    => 'Customizer advanced settings',
			'priority' => 1
		)
	);


	$wp_customize->add_setting(
		'wst_activate_options',
		array(
			'default'    => 0,

		)
	);

	$wp_customize->add_control(
		'wst_activate_options',
		array(
			'section'    => 'wst_additional_options',
			'label'      => 'Activate advanced options?',
			'type'       => 'checkbox'
		)
	);


}
add_action( 'customize_register', 'wst_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function wst_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function wst_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function wst_customize_preview_js() {
	wp_enqueue_script( 'wst-customizer', get_template_directory_uri() . 'assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'wst_customize_preview_js' );

/**
 * Add the theme configuration
 */
wstCustomizer::add_config( 'webstantly_theme', array(
	'option_type' => 'theme_mod',
	'capability'  => 'edit_theme_options',
) );

/**
 * Add the typography section
 */
wstCustomizer::add_section( 'typography', array(
	'title'      => esc_attr__( 'Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 2,
	'capability' => 'edit_theme_options',
) );

/**
 * Add the body-typography control
 */
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'typography',
	'settings'    => 'body_typography',
	'label'       => esc_attr__( 'Body Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the main typography options for your site.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here apply to all content on your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'typography',
	'transport' => 'postMessage',
	'priority'    => 10,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		 'letter-spacing' => '0',
		'color'          => '#333333',
	),
	'js_vars' => array(
		array(
			'element'=>'body',
			'function'=>'css',
			'property'=>'color, font-size, line-height, font-weight, letter-spacing'
		)

	),
	'output' => array(
		array(
			'element' => 'body',
		),
	),
) );

/**
 * Add the header-typography control
 */
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'typography',
	'settings'    => 'headers_typography',
	'label'       => esc_attr__( 'Headers Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the typography options for your headers.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here will override the Body Typography options for all headers on your site (post titles, widget titles etc).', CHILD_TEXT_DOMAIN ),
	'section'     => 'typography',
	'transport' => 'postMessage',
	'priority'    => 11,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
//		 'font-size'      => '16px',
		 'line-height'    => '1.5',
		 'letter-spacing' => '0',
		 'color'          => '#333333',
		'text-transform'          => '',
	),
	'js_vars' => array(
		array(
			'element'=>'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, h1 a,h2 a, h3 a, h4 a, h5 a, h6 a',
			'function'=>'css',
			'property'=>'color, font-size, line-height, font-weight, letter-spacing, text-transform'
		)
	),
	'output' => array(
		array(
			'element' => array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', '.h1', '.h2', '.h3', '.h4', '.h5', '.h6', 'h1 a','h2 a', 'h3 a', 'h4 a', 'h5 a', 'h6 a' ),
		),
	),
) );

/**
 * Navigation typography
 */
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'typography',
	'settings'    => 'vav_typography',
	'label'       => esc_attr__( 'Navigation Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the navigations typography options for your site.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here apply to all navigations on your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'typography',
	'transport' => 'postMessage',
	'priority'    => 12,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '700',
		'font-size'      => '11px',
		'line-height'    => '1.5',
		'letter-spacing' => '1',
		'color'          => '#333333',
		'text-transform'          => 'uppercase',
	),
	'js_vars' => array(
		array(
			'element'=>'.genesis-nav-menu a',
			'function'=>'css',
			'property'=>'color, font-size, line-height, font-weight, letter-spacing, text-transform'
		)

	),
	'output' => array(
		array(
			'element' => '.genesis-nav-menu a',
		),
	),
) );