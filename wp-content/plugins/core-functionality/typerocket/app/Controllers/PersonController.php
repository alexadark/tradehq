<?php
namespace App\Controllers;

use \App\Models\Person;
use \TypeRocket\Controllers\WPPostController;

class PersonController extends WPPostController
{
    protected $modelClass = Person::class;
}