<?php
/**
 * Plugin Name: Core Functionality
 * Plugin URI: https://github.com/billerickson/Core-Functionality
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 2.0.0
 * Author: Bill Erickson
 * Author URI: http://www.billerickson.net
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

// Plugin Directory 
define( 'WST_DIR', dirname( __FILE__ ) );

require_once( WST_DIR . '/inc/metaboxes.php'      );

require( 'typerocket/init.php' );

$slider = tr_meta_box('Slider');
$slider->addScreen( 'page' );


function add_meta_content_slider() {

	$form = tr_form();
	echo $form->repeater( 'Slides' )->setFields(
		array(
			$form->image( 'Slide Image' ),
			$form->textarea( 'Slide Text' ),
			$form->textarea( 'Slide Subtext' ),
		)
	);



}

$components = tr_meta_box('Page Components');
$components->addScreen('page');

function add_meta_content_page_components() {
	$form = tr_form();
	echo $form->matrix('Components');
}







